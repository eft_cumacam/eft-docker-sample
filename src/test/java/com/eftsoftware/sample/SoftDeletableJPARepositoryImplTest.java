package com.eftsoftware.sample;

import com.eftsoftware.sample.domain.Sample;
import com.eftsoftware.sample.repository.SampleRepository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SampleApplication.class)
public class SoftDeletableJPARepositoryImplTest {

	@Autowired
	private SampleRepository repo;

	private Sample sample = new Sample("dumy");

	@Before
	public void setUp() {
	}

	@After
	public void tearDown(){

	}

	@Test
	public void test(){
		Sample actual = repo.save(sample);
		Assert.assertEquals(actual, sample);
		repo.deleteById(actual.getId());
		Sample deleted = repo.loadActiveById(actual.getId());
		Assert.assertNull(deleted);
	}
}
