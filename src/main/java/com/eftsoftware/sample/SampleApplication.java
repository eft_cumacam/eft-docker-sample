package com.eftsoftware.sample;

import java.util.Collections;

import com.eftsoftware.common.config.JPAConfig;
import com.eftsoftware.common.config.JerseyConfig;
import com.eftsoftware.common.filter.ContextFilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@ComponentScan(value = {"com.eftsoftware.common.featuretoggle","com.eftsoftware.sample"})
@Import({ JerseyConfig.class, JPAConfig.class})
public class SampleApplication {

	@Bean
	ContextFilter contextFilter() {
		return new ContextFilter(Collections.emptyList());
	}

	public static void main(String[] args) {
		SpringApplication.run(SampleApplication.class, args);
	}
}
