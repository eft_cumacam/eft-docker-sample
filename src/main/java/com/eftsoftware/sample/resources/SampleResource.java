package com.eftsoftware.sample.resources;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import com.eftsoftware.common.featuretoggle.FeatureToggle;
import com.eftsoftware.sample.domain.Sample;
import com.eftsoftware.sample.dto.SampleDTO;
import com.eftsoftware.sample.service.SampleService;

import org.springframework.beans.factory.annotation.Autowired;

@Provider
@Path("/samples")
@Produces(MediaType.APPLICATION_JSON)
public class SampleResource {

    private SampleService service;

    @Autowired
    public SampleResource(SampleService service) {
        this.service = service;
    }

    @POST
    @Path("/")
    @FeatureToggle(feature = "feature.sample.create")
    public Sample create(SampleDTO dto) {
        Sample sample = new Sample(dto.getValue());
        return service.save(sample);
    }

    @GET
    @Path("/{id}")
    @FeatureToggle(feature = "feature.sample.get")
    public Sample get(@PathParam("id") long id) {
        return service.loadActiveById(id);
    }

    @DELETE
    @Path("/{id}")
    @FeatureToggle(feature = "feature.sample.delete")
    public void delete(@PathParam("id") long id) {
        service.deleteById(id);
    }

    @PUT
    @Path("/{id}")
    @FeatureToggle(feature = "feature.sample.update")
    public Sample update(@PathParam("id") long id, @Valid @NotNull Sample sample) {
        return service.save(sample);
    }
}
