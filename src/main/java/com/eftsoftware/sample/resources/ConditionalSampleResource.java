package com.eftsoftware.sample.resources;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import com.eftsoftware.common.featuretoggling.FeatureToggle;

/**
 * Created by cumacam on 15/02/2017.
 */
@Provider
@Path("/conditional")
@Produces(MediaType.APPLICATION_JSON)
@FeatureToggle(feature = "feature.conditional.allresource")
public class ConditionalSampleResource {

    @GET
    @Path("/")
    public String sayHello(){
        return "Hello bro!";
    }
}
