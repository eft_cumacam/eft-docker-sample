package com.eftsoftware.sample.repository;

import com.eftsoftware.common.repository.SoftDeletableJPARepository;
import com.eftsoftware.sample.domain.Sample;

import org.springframework.stereotype.Repository;

@Repository
public interface SampleRepository extends SoftDeletableJPARepository<Sample, Long> {

	Sample findByValue(String value);
}
