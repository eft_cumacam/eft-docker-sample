package com.eftsoftware.sample.dto;


import com.eftsoftware.common.domain.BaseObject;
import lombok.Data;

@Data
public class SampleDTO extends BaseObject {
    private String value;
}
