package com.eftsoftware.sample.domain;

import javax.persistence.Entity;

import com.eftsoftware.common.domain.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Sample extends BaseEntity<Long> {

    private String value;
}
