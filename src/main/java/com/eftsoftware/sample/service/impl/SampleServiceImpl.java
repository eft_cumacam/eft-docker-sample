package com.eftsoftware.sample.service.impl;

import com.eftsoftware.common.service.AbstractSoftDeletableJPAServiceImpl;
import com.eftsoftware.sample.domain.Sample;
import com.eftsoftware.sample.repository.SampleRepository;
import com.eftsoftware.sample.service.SampleService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SampleServiceImpl extends AbstractSoftDeletableJPAServiceImpl<Sample, Long> implements SampleService {

    private SampleRepository repository;

    @Autowired
    public SampleServiceImpl(SampleRepository repository) {
        super(repository);
        this.repository = repository;
    }
}
