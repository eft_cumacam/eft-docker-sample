package com.eftsoftware.sample.service;


import com.eftsoftware.common.service.SoftDeletableService;
import com.eftsoftware.sample.domain.Sample;

public interface SampleService extends SoftDeletableService<Sample, Long> {
}
